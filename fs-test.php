<?php

function createConditional($dirName)
{
    if (is_dir($dirName)) {
        shell_exec("mkdir -p $dirName");
    }
}

function allwaysCreate($dirName)
{
    shell_exec("mkdir -p $dirName");
}

function createConditionalPhp($dirName)
{
    if (is_dir($dirName)) {
        mkdir($dirName);
    }
}

function allwaysCreatePhp($dirName)
{
    mkdir($dirName);
}

$time_start = microtime(true);
for ($i = 0; $i < 1000; $i++) {
    allwaysCreate(__DIR__ . DIRECTORY_SEPARATOR . "test" . DIRECTORY_SEPARATOR . uniqid("", true));
}

$time_end = microtime(true);
$time1 = $time_end - $time_start;


$time_start = microtime(true);
for ($i = 0; $i < 1000; $i++) {
    createConditional(__DIR__ . DIRECTORY_SEPARATOR . "test" . DIRECTORY_SEPARATOR . uniqid("", true));
}

$time_end = microtime(true);
$time2 = $time_end - $time_start;

$time_start = microtime(true);
for ($i = 0; $i < 1000; $i++) {
    allwaysCreatePhp(__DIR__ . DIRECTORY_SEPARATOR . "test" . DIRECTORY_SEPARATOR . uniqid("", true));
}

$time_end = microtime(true);
$time3 = $time_end - $time_start;

$time_start = microtime(true);
for ($i = 0; $i < 1000; $i++) {
    createConditionalPhp(__DIR__ . DIRECTORY_SEPARATOR . "test" . DIRECTORY_SEPARATOR . uniqid("", true));
}

$time_end = microtime(true);
$time4 = $time_end - $time_start;


print "Always try to create folder: " . $time1 . "\n";
print "Check before try to create folder: " . $time2 . "\n";
print "Create folder by php: " . $time3 . "\n";
print "Check before create folder by php: " . $time4 . "\n";

/**
 * Always try to create folder: 3.1996448040009
 * Check before try to create folder: 0.0064330101013184
 * Create folder by php: 0.065963983535767
 * Check before create folder by php: 0.011148929595947
 */
